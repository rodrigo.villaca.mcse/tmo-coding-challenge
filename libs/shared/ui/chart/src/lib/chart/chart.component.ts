import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit
} from '@angular/core';
import { ChartOptions } from './chart-options.interface';
import { DEFAULT_CHART_OPTIONS } from './default-chart-options.constant';

@Component({
  selector: 'coding-challenge-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChartComponent implements OnInit {
  @Input() data: (string | number)[][];
  @Input() options: Partial<ChartOptions>;
  chartOptions: ChartOptions;

  constructor() {}

  ngOnInit() {
    // REMARKS: Example of merging options
    // Here I am using spread operator, but as we are using a deep options object we need to go manually to down level properties as spread and Object.assign does a shallow clone/merge
    // normally I would deepmerge or something similar for this, but as this quick example will just use the title property using spread.
    if (this.options) {
      this.chartOptions = { ...DEFAULT_CHART_OPTIONS, ...this.options };
      if (this.options.title) {
        this.chartOptions.options.title = this.options.title;
      }
    } else {
      this.chartOptions = DEFAULT_CHART_OPTIONS;
    }
  }
}
