import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ChartOptions } from '@coding-challenge/shared/ui/chart';
import { PriceQueryFacade } from '@coding-challenge/stocks/data-access-price-query';
import { SymbolValidator } from '@coding-challenge/stocks/data-validation';
import { BehaviorSubject, Subscription } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  filter,
  first,
  flatMap,
  map,
  startWith
} from 'rxjs/operators';
import { TIME_PERIODS } from './time-periods.constant';
import { TITLE } from './title.constant';

@Component({
  selector: 'coding-challenge-stocks',
  templateUrl: './stocks.component.html',
  styleUrls: ['./stocks.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StocksComponent implements OnInit, OnDestroy {
  private subscriptions: Subscription[] = [];
  private today = new Date();

  // REMARKS: task3 - NgRx might have something more appropriate for filtering (like AkitaFiltersPlugin), for the example using plain rxjs
  private filterDateSubject = new BehaviorSubject<{ start: Date; end: Date }>(
    null
  );

  period: string;
  filterDate$ = this.filterDateSubject.asObservable();

  quotes$ = this.filterDate$.pipe(
    flatMap(filterDates => {
      return this.priceQuery.priceQueries$.pipe(
        map(quotes => {
          if (!filterDates || !filterDates.start || !filterDates.end) {
            return quotes;
          } else {
            return quotes.filter(
              quote =>
                new Date(quote[0]) >= filterDates.start &&
                new Date(quote[0]) <= filterDates.end
            );
          }
        })
      );
      // }
    })
  );

  // REMARKS: task3 - normally I would use moment, but for the example I am using JS Date, with moment we would not need a lot of this code
  dates$ = this.priceQuery.priceQueries$.pipe(
    map(quotes =>
      quotes.map(quote => {
        const dateString = quote[0] as string;
        const date = new Date(`${dateString}T00:00:00.000`);

        return date;
      })
    )
  );

  maxDate$ = this.dates$.pipe(
    map(dates =>
      dates && dates.length
        ? dates.reduce((a, b) => {
            return a > b ? a : b;
          })
        : this.today
    ),
    map(date => {
      date.setHours(23, 59, 59, 999);
      return date;
    })
  );
  minDate$ = this.dates$.pipe(
    map(dates =>
      dates && dates.length
        ? dates.reduce((a, b) => {
            return a < b ? a : b;
          })
        : dates
    )
  );

  stockPickerForm: FormGroup;
  symbol: string;
  timePeriods = TIME_PERIODS;

  // REMARKS: added ngrx loading indicator
  loading$ = this.priceQuery.loading$;

  // REMARKS: finished implementing selectedSymbol - query
  selectedSymbol$ = this.priceQuery.selectedSymbol$;

  // REMARKS: For the example I am changing the default title, without changing the other default options
  options: Partial<ChartOptions> = { title: TITLE };

  // REMARKS: query errors
  error$ = this.priceQuery.error$;

  // REMARKS: task3 - separate form group
  dateRangeForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private priceQuery: PriceQueryFacade,
    private symbolValidator: SymbolValidator
  ) {}

  private initializeForm() {
    this.stockPickerForm = this.fb.group({
      // REMARKS: here I added the validateSymbol async validator
      symbol: [
        null,
        {
          validators: [Validators.required],
          asyncValidators: [this.symbolValidator.validateSymbol()]
        }
      ],
      period: [null, Validators.required]
    });

    // REMARKS: task3 - I created a separate form as this will drive a separate action, this way we can listen for form wide valueChanges instead of having it set for each item
    this.dateRangeForm = this.fb.group({
      startDate: [null],
      endDate: [null]
    });

    // REMARKS: task2 - deal with unsubscribing the subscription, normally I would use untilDestroyed, but doing this to be explicit on this challenge
    this.subscriptions.push(
      // REMARKS: task2 - use valueChanges subscription to make it more elegant and easier to test
      this.stockPickerForm.valueChanges
        .pipe(
          flatMap(() =>
            this.stockPickerForm.statusChanges.pipe(
              startWith(this.stockPickerForm.status),
              filter(status => status !== 'PENDING'),
              debounceTime(500),
              first()
            )
          )
        )
        .subscribe(() => {
          this.fetchQuote();
        }),
      // REMARKS: task3 - use valueChanges to update the filter
      this.dateRangeForm.valueChanges
        .pipe(distinctUntilChanged())
        .subscribe(() => {
          const controls = this.dateRangeForm.controls;
          if (
            !controls.startDate ||
            !controls.startDate.value ||
            !controls.endDate ||
            !controls.endDate.value
          ) {
            this.filterDateSubject.next(null);
          } else {
            const start = controls.startDate.value;
            const end = controls.endDate.value;

            this.filterDateSubject.next({ start, end });
          }
        })
    );
  }

  ngOnInit() {
    this.subscriptions = [];
    this.initializeForm();
  }

  // REMARKS: task2 - deal with unsubscribing the subscription, normally I would use untilDestroyed, but doing this to be explicit on this challenge
  ngOnDestroy() {
    this.subscriptions.forEach(subscription => subscription.unsubscribe());
  }

  fetchQuote() {
    if (this.stockPickerForm.valid) {
      const { symbol, period } = this.stockPickerForm.value;
      this.priceQuery.fetchQuote(symbol, period);

      // REMARKS: finished implementing selectedSymbol - dispatch a new SelectSymbol action
      this.priceQuery.selectSymbol(symbol);

      // REMARKS: task3 - reset the filter form and subject on querying new data
      this.filterDateSubject.next(null);
      this.dateRangeForm.reset(undefined, { emitEvent: false });
    }
  }
}
