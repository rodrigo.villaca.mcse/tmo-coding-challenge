import { Injectable } from '@angular/core';
import { ApiService } from '@coding-challenge/stocks/data-access-api';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { DataPersistence } from '@nrwl/nx';
import { map } from 'rxjs/operators';
import {
  FetchPriceQuery,
  PriceQueryActionTypes,
  PriceQueryFetched,
  PriceQueryFetchError,
  SelectSymbol,
  SelectSymbolSuccess
} from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import { PriceQueryResponse } from './price-query.type';

@Injectable()
export class PriceQueryEffects {
  @Effect() loadPriceQuery$ = this.dataPersistence.fetch(
    PriceQueryActionTypes.FetchPriceQuery,
    {
      run: (action: FetchPriceQuery, state: PriceQueryPartialState) => {
        // REMARKS: using the new api service that wraps the http call, effects don't need to know about http and urls
        return this.apiService
          .fetchQuote(action.symbol, action.period)
          .pipe(
            map(resp => new PriceQueryFetched(resp as PriceQueryResponse[]))
          );
      },

      onError: (action: FetchPriceQuery, error) => {
        return new PriceQueryFetchError(error);
      }
    }
  );

  // REMARKS: finished implementing selectedSymbol - effect
  @Effect() loadSelectedSymbol$ = this.actions$.pipe(
    ofType<SelectSymbol>(PriceQueryActionTypes.SelectSymbol),
    map(action => new SelectSymbolSuccess(action.symbol))
  );

  constructor(
    private actions$: Actions,
    private apiService: ApiService,
    private dataPersistence: DataPersistence<PriceQueryPartialState>
  ) {}
}
