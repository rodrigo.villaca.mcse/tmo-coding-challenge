module.exports = {
  name: 'stocks-data-access-api',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/stocks/data-access-api',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
