import { of } from 'rxjs';

export class PriceQueryFacadeMock {
  priceQueries$ = of([[]]);
  loading$ = of(false);
  selectedSymbol$ = of(null);
  error$ = of(null);
  fetchQuote = (symbol, period) => {};
  selectSymbol = symbol => {};
}
