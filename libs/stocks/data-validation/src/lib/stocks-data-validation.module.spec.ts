import { async, TestBed } from '@angular/core/testing';
import { StocksDataValidationModule } from './stocks-data-validation.module';

describe('StocksDataValidationModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StocksDataValidationModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(StocksDataValidationModule).toBeDefined();
  });
});
