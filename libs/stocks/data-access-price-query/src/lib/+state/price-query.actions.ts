import { Action } from '@ngrx/store';
import { PriceQueryResponse } from './price-query.type';

// REMARKS: finished implementing selectedSymbol - success and error actions several places in this file
export enum PriceQueryActionTypes {
  SelectSymbol = 'priceQuery.selectSymbol',
  SelectSymbolSuccess = 'priceQuery.selectSymbolSuccess',
  SelectSymbolError = 'priceQuery.selectSymbolError',
  FetchPriceQuery = 'priceQuery.fetch',
  PriceQueryFetched = 'priceQuery.fetched',
  PriceQueryFetchError = 'priceQuery.error'
}

export class FetchPriceQuery implements Action {
  readonly type = PriceQueryActionTypes.FetchPriceQuery;
  constructor(public symbol: string, public period: string) {}
}

export class PriceQueryFetchError implements Action {
  readonly type = PriceQueryActionTypes.PriceQueryFetchError;
  constructor(public error: any) {}
}

export class PriceQueryFetched implements Action {
  readonly type = PriceQueryActionTypes.PriceQueryFetched;
  constructor(public queryResults: PriceQueryResponse[]) {}
}
export class SelectSymbol implements Action {
  readonly type = PriceQueryActionTypes.SelectSymbol;
  constructor(public symbol: string) {}
}

export class SelectSymbolSuccess implements Action {
  readonly type = PriceQueryActionTypes.SelectSymbolSuccess;
  constructor(public symbol: string) {}
}
export class SelectSymbolError implements Action {
  readonly type = PriceQueryActionTypes.SelectSymbolError;
  constructor(public symbol: string) {}
}

export type PriceQueryAction =
  | FetchPriceQuery
  | PriceQueryFetched
  | PriceQueryFetchError
  | SelectSymbol
  | SelectSymbolSuccess
  | SelectSymbolError;

export const fromPriceQueryActions = {
  FetchPriceQuery,
  PriceQueryFetched,
  PriceQueryFetchError,
  SelectSymbol,
  SelectSymbolSuccess,
  SelectSymbolError
};
