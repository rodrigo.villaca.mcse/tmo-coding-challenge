module.exports = {
  name: 'stocks-data-validation',
  preset: '../../../jest.config.js',
  coverageDirectory: '../../../coverage/libs/stocks/data-validation',
  snapshotSerializers: [
    'jest-preset-angular/AngularSnapshotSerializer.js',
    'jest-preset-angular/HTMLCommentSerializer.js'
  ]
};
