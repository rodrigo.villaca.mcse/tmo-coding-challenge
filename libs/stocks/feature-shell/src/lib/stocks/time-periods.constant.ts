export const TIME_PERIODS = [
  { viewValue: 'All available data', value: 'max' },
  { viewValue: 'Five years', value: '5y' },
  { viewValue: 'Two years', value: '2y' },
  { viewValue: 'One year', value: '1y' },
  { viewValue: 'Year-to-date', value: 'ytd' },
  { viewValue: 'Six months', value: '6m' },
  { viewValue: 'Three months', value: '3m' },
  { viewValue: 'One month', value: '1m' }
];
