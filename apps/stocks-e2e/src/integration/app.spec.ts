import { getGreeting } from '../support/app.po';

describe('Stocks e2e', () => {
  beforeEach(() => cy.visit('/'));

  it('should display welcome message', () => {
    getGreeting().contains('Welcome to stocks!');
  });
  it('should render chart', () => {
    cy.get('input[formControlName=symbol]')
      .click()
      .type('TMUS');

    cy.get('mat-select[formControlName=period]')
      .click()
      .get('mat-option')
      .contains('One month')
      .click();

    cy.wait(5000);
    cy.get('google-chart').should('exist');
  });
});
