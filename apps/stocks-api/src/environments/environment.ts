// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
import { StocksAppConfig } from '@coding-challenge/stocks/data-access-app-config';

export const environment: StocksAppConfig & {
  ttl: number;
  port: number;
  host: string;
} = {
  production: false,
  apiKey: 'Tpk_226ff0c140af410cb63f9f27812a2bbf',
  apiURL: 'https://sandbox.iexapis.com',
  ttl: 30 * 60 * 1000,
  port: 3333,
  host: 'localhost'
};
