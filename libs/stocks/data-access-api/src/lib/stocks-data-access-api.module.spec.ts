import { async, TestBed } from '@angular/core/testing';
import { StocksDataAccessApiModule } from './stocks-data-access-api.module';

describe('StocksDataAccessApiModule', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [StocksDataAccessApiModule]
    }).compileComponents();
  }));

  it('should create', () => {
    expect(StocksDataAccessApiModule).toBeDefined();
  });
});
