import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import {
  async,
  ComponentFixture,
  fakeAsync,
  TestBed
} from '@angular/core/testing';
import { FormGroup, ReactiveFormsModule } from '@angular/forms';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatSelectModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatExpansionModule,
  MatCardModule,
  MatDividerModule
} from '@angular/material';

import { By } from '@angular/platform-browser';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import {
  ChartComponent,
  SharedUiChartModule
} from '@coding-challenge/shared/ui/chart';
import {
  PriceQueryFacade,
  PriceQueryFacadeMock
} from '@coding-challenge/stocks/data-access-price-query';
import {
  SymbolValidator,
  SymbolValidatorMock
} from '@coding-challenge/stocks/data-validation';
import { StoreModule } from '@ngrx/store';
import { StocksComponent } from './stocks.component';
import { TIME_PERIODS } from './time-periods.constant';
let httpMock: HttpTestingController;

describe('StocksComponent', () => {
  let component: StocksComponent;
  let fixture: ComponentFixture<StocksComponent>;
  let chartElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        MatFormFieldModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatInputModule,
        MatSelectModule,
        MatButtonModule,
        MatDatepickerModule,
        MatNativeDateModule,
        MatExpansionModule,
        MatCardModule,
        MatDividerModule,
        SharedUiChartModule,
        StoreModule.forRoot({}),
        NoopAnimationsModule,
        HttpClientTestingModule
      ],
      providers: [
        { provide: PriceQueryFacade, useClass: PriceQueryFacadeMock },
        { provide: SymbolValidator, useClass: SymbolValidatorMock }
      ],
      declarations: [StocksComponent]
    });
    httpMock = TestBed.get(HttpTestingController);

    TestBed.compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StocksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    chartElement = fixture.debugElement.query(By.css('coding-challenge-chart'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize', () => {
    component.stockPickerForm = null;
    component.ngOnInit();

    expect(component.quotes$).toBeDefined();
    expect(component.stockPickerForm.controls.symbol).toBeDefined();
    expect(component.stockPickerForm.controls.period).toBeDefined();
  });

  describe('form tests', () => {
    beforeEach(() => {
      component.ngOnInit();
      component.stockPickerForm.reset();
    });

    it('should initialize form', () => {
      expect(component.stockPickerForm).toBeInstanceOf(FormGroup);
    });

    describe('invalid form input', () => {
      it('should handle invalid form group', () => {
        expect(component.stockPickerForm.invalid).toBeTruthy();
        expect(component.stockPickerForm.errors).toBeDefined();
      });

      it('should handle invalid symbol form control', () => {
        const control = component.stockPickerForm.controls.symbol;

        expect(control.value).toBeNull();
        expect(control.invalid).toBeTruthy();
        expect(control.errors).toEqual({
          required: true
        });
      });

      it('should handle invalid period form control', () => {
        const control = component.stockPickerForm.controls.period;

        expect(control.value).toBeNull();
        expect(control.invalid).toBeTruthy();
        expect(control.errors).toEqual({
          required: true
        });
      });
    });
    describe('valid form input', () => {
      const symbol = 'TMUS';
      const period = TIME_PERIODS.find(item => item.value === '1m');
      beforeEach(() => {
        component.stockPickerForm.patchValue({ symbol, period });
      });

      it('should handle valid form group', fakeAsync(() => {
        // tick(3000);
        expect(component.stockPickerForm.valid).toBeTruthy();
        expect(component.stockPickerForm.errors).toBeNull();
      }));

      it('should handle valid symbol form control', () => {
        const control = component.stockPickerForm.controls.symbol;
        expect(control.value).toEqual(symbol);
        expect(control.valid).toBeTruthy();
        expect(control.errors).toBeNull();
      });

      it('should handle valid period form control', () => {
        const control = component.stockPickerForm.controls.period;

        expect(control.value).toEqual(period);
        expect(control.valid).toBeTruthy();
        expect(control.errors).toBeNull();
      });

      it('should set options for chart component', () => {
        const chart = chartElement.componentInstance as ChartComponent;
        expect(chart).toBeInstanceOf(ChartComponent);
      });

      it('should call fetch on value changes ', done => {
        const control = component.stockPickerForm.controls.symbol;
        const fetchSpy = spyOn(component, 'fetchQuote').and.callThrough();
        component.ngOnInit();

        expect(fetchSpy).not.toHaveBeenCalled();
        component.stockPickerForm.patchValue({ symbol: 'test', period });

        fixture.detectChanges();
        control.setValue(symbol);
        fixture.detectChanges();
        setTimeout(() => {
          expect(fetchSpy).toHaveBeenCalled();
          done();
        }, 4000);
      });

      it('should handle valid start/end date', () => {
        const startControl = component.dateRangeForm.controls.startDate;
        const endControl = component.dateRangeForm.controls.endDate;

        component.ngOnInit();
        startControl.setValue(new Date('2010-10-10'));
        endControl.setValue(new Date('2010-10-15'));
        fixture.detectChanges();
        expect(startControl.valid).toBe(true);
        expect(endControl.valid).toBe(true);
        expect(component.dateRangeForm.valid).toBe(true);
      });
    });
  });
});
