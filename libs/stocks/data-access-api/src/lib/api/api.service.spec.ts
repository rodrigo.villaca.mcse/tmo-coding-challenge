import {
  HttpClientTestingModule,
  HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import {
  mockEnvironment,
  StocksAppConfigToken
} from '@coding-challenge/stocks/data-access-app-config';
import { ApiService } from './api.service';

const mockFetchQuoteResponse = [
  {
    date: '2020-01-02',
    open: 82.81,
    close: 79.45,
    high: 79.9,
    low: 80.48,
    volume: 3549745,
    uOpen: 79.55,
    uClose: 79.68,
    uHigh: 79.16,
    uLow: 81.14,
    uVolume: 3411608,
    change: 0,
    changePercent: 0,
    label: 'Jan 2, 20',
    changeOverTime: 0
  },
  {
    date: '2020-01-03',
    open: 80.78,
    close: 78.68,
    high: 81.15,
    low: 81.21,
    volume: 1834999,
    uOpen: 81.86,
    uClose: 81.07,
    uHigh: 78.7,
    uLow: 81.16,
    uVolume: 1844423,
    change: -0.42,
    changePercent: -0.5557,
    label: 'Jan 3, 20',
    changeOverTime: -0.005524
  }
];
describe('ApiService', () => {
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [{ provide: StocksAppConfigToken, useValue: mockEnvironment }]
    });
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    const service: ApiService = TestBed.get(ApiService);
    expect(service).toBeTruthy();
  });

  it('should call validateSymbol for valid symbol', done => {
    const service: ApiService = TestBed.get(ApiService);
    const symbol = 'TMUS';
    const url = `${mockEnvironment.apiURL}/quote/${symbol}`;
    service.validateSymbol(symbol).subscribe(
      response => {
        expect(response).toBe(true);
        done();
      },
      error => done(error)
    );

    const req = httpMock.expectOne(url);
    expect(req.request.method).toEqual('GET');

    // REMARKS: validateSymbol does not care about the data because 'map(response => !!response)' so just setting response a random object
    req.flush({ data: {} });
  });

  it('should call validateSymbol for invalid symbol', done => {
    const service: ApiService = TestBed.get(ApiService);
    const symbol = 'invalid';
    const url = `${mockEnvironment.apiURL}/quote/${symbol}`;

    service.validateSymbol(symbol).subscribe(
      response => {
        expect(response).toBe(false);
        done();
      },
      error => done(error)
    );

    const req = httpMock.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush('Unknown symbol', { status: 404, statusText: 'Not Found' });
  });

  it('should call fetchQuote for valid symbol', done => {
    const service: ApiService = TestBed.get(ApiService);
    const symbol = 'TMUS';
    const period = 'ytd';
    const url = `${mockEnvironment.apiURL}/chart/${symbol}/${period}`;

    service.fetchQuote(symbol, period).subscribe(
      response => {
        expect(response).toEqual(mockFetchQuoteResponse);
        done();
      },
      error => done(error)
    );

    const req = httpMock.expectOne(url);
    expect(req.request.method).toEqual('GET');

    req.flush(mockFetchQuoteResponse);
  });

  it('should call fetchQuote for invalid symbol', done => {
    const service: ApiService = TestBed.get(ApiService);
    const symbol = 'invalid';
    const period = 'ytd';
    const url = `${mockEnvironment.apiURL}/chart/${symbol}/${period}`;

    service.fetchQuote(symbol, period).subscribe(
      () => {
        done('should return an error');
      },
      () => done()
    );

    const req = httpMock.expectOne(url);
    expect(req.request.method).toEqual('GET');
    req.flush('Unknown symbol', { status: 404, statusText: 'Not Found' });
  });
});
