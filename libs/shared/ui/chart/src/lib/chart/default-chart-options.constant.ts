import { ChartOptions } from './chart-options.interface';

export const DEFAULT_CHART_OPTIONS: ChartOptions = {
  title: '',
  type: 'LineChart',
  data: [],
  columnNames: ['period', 'close'],
  options: { title: 'Stock price', width: 600, height: 400 }
};
