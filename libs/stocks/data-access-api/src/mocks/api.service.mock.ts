import { of } from 'rxjs';

export class ApiServiceMock {
  validateSymbol(symbol: string) {
    // REMARKS: if the symbol is not TMUS we mock a failed validation
    if (symbol === 'TMUS') {
      return of(true);
    } else {
      return of(false);
    }
  }
}
