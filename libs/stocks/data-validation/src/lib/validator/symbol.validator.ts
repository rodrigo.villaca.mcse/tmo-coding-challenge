import { Injectable } from '@angular/core';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { ApiService } from '@coding-challenge/stocks/data-access-api';
import { Observable, of } from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  first,
  map,
  switchMap
} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SymbolValidator {
  constructor(private apiService: ApiService) {}

  validateSymbol(): AsyncValidatorFn {
    return (
      control: AbstractControl
    ):
      | Promise<{ [key: string]: any } | null>
      | Observable<{ [key: string]: any } | null> => {
      if (!control || !control.value) {
        return of(null);
      } else {
        return control.valueChanges.pipe(
          debounceTime(500),
          distinctUntilChanged(),
          first(),
          switchMap(value =>
            this.apiService
              .validateSymbol(value)
              .pipe(
                map(valid =>
                  valid ? null : { invalidSymbol: { value: value } }
                )
              )
          )
        );
      }
    };
  }
}
