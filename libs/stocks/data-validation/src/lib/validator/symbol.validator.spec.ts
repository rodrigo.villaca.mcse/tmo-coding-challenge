import { TestBed, async } from '@angular/core/testing';
import { StocksAppConfigToken } from '@coding-challenge/stocks/data-access-app-config';
import { SymbolValidator } from './symbol.validator';
import {
  ApiService,
  ApiServiceMock
} from '@coding-challenge/stocks/data-access-api';
import { FormControl } from '@angular/forms';
import { from, isObservable } from 'rxjs';

describe('SymbolValidator', () => {
  beforeEach(() =>
    TestBed.configureTestingModule({
      providers: [{ provide: ApiService, useClass: ApiServiceMock }]
    })
  );
  it('should be created', () => {
    const service: SymbolValidator = TestBed.get(SymbolValidator);
    expect(service).toBeTruthy();
  });

  it('should handle valid input', done => {
    const service: SymbolValidator = TestBed.get(SymbolValidator);
    const validationFn = service.validateSymbol();
    const control = new FormControl();
    control.setValue('TMUS');
    const validationFnResult = validationFn(control);

    if (isObservable(validationFnResult)) {
      validationFnResult.subscribe(
        result => {
          expect(result).toBeNull();
          done();
        },
        () => {
          done('unexpected error');
        }
      );
      control.setValue('TMUS');
    } else {
      done('non-observable returned by async validator fn');
    }
  });

  it('should handle invalid input', done => {
    const service: SymbolValidator = TestBed.get(SymbolValidator);
    const validationFn = service.validateSymbol();
    const control = new FormControl();
    control.setValue('invalid');
    const validationFnResult = validationFn(control);

    if (isObservable(validationFnResult)) {
      validationFnResult.subscribe(
        result => {
          expect(
            result && result.invalidSymbol && result.invalidSymbol.value
          ).toEqual('invalid');
          done();
        },
        () => {
          done('unexpected error');
        }
      );
      control.setValue('invalid');
    } else {
      done('non-observable returned by async validator fn');
    }
  });
});
