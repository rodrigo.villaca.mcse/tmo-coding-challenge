# T-Mobile Coding Challenge

### Important! Read this First !

Do **not** submit a pull request to this repository. You PR wil be rejected and your submission ignored.

To _properly_ submit a coding challenge you must:

1. fork this repository
2. make the necessary changes
3. push changes to your forked origin
4. send address of your fork to t-mobile.

We will review your fork online before and during your interview.

# Stocks coding challenge

## How to run the application

There are two apps: `stocks` and `stocks-api`.

- `stocks` is the front-end. It uses Angular 7 and Material. You can run this using `yarn serve:stocks`
- `stocks-api` uses Hapi and has a very minimal implementation. You can start the API server with `yarn serve:stocks-api`

A proxy has been set up in `stocks` to proxy calls to `localhost:3333` which is the port that the Hapi server listens on.

> You need to register for a token here: https://iexcloud.io/cloud-login#/register/ Use this token in the `environment.ts` file for the `stocks` app.

> The charting library is the Google charts API: https://developers.google.com/chart/

## Problem statement

[Original problem statement](https://bitbucket.org/kburson3/developer-puzzles/src/3fb1841175cd567a63abfbe18c08e4d2a734c2e9/puzzles/web-api/stock-broker.md)

### Task 1

Please provide a short code review of the base `master` branch:

1. What is done well?
2. What would you change?
3. Are there any code smells or problematic implementations?

> Make a PR to fix at least one of the issues that you identify

#### Solution

I also added comments to explain some code. Search for `REMARKS:` for seeing those explanations.

1. What is done well?

- Separating components
- Nwrl monorepo is really nice
- Using prettier
- Using a state management library
  - modern state management libraries like NgRx, Akita, NGXS provide redux tools support and integrate well into angular by using observables
- Using lazy loading is generally good.
  - but it depends a lot on the app usage, in some cases we might want to use more device memory for a faster navigation.
    - For those cases I would write a custom router preloading strategy to load the app fast and asynchronously load the other modules.
    - That's a common case for mobile apps as loading a module can take long and affect the user experience

2. What would you change?

- What I changed/fixed:

  - Items described on question 3.
  - Existing tests are failing
    - import RouterTestingModule module at app.component.spec to fix router-outlet error
    - added title to app.component ts and html to fix title and h1 errors
    - import google charts module at chart component spec to fix template parse error
    - imports and providers at stocks.components.spec fixing multiple dependency errors
    - ng test was giving an error exit code, so I added required tests to stocks-app-data.spec.ts and stocks-api.spec.ts.
    - made stocks-api/src/main.ts testable by moving the server const outside of the init function.
  - Lack of tests
    - wrote tests for stocks and chart components, I would recommend also writing tests for the ngrx related classes.
    - wrote one more e2e test that check select values on the form and check that the chart is rendered.
  - Use scss instead of plain css
  - Wrapped http calls in apiService
    - normally this provide an easier way to deal with caching, central config, etc.
    - also is a good practice to separate concerns so the ngrx effect does not need to know about the url
    - see remarks at api.service.ts
  - Use constants for default values - or json files (required for i18n in some projects I worked)
    - for this example I am using constants, one per file but we could have a shared constant files with many different values depending on the architecture of the app.
  - Separate routing code in it's own files
    - Also we are using `loadChildren:` as string, and that's deprecated on angular 8
    - In most larger apps we might want to have a custom route preloading strategy, for example, in a mobile app I work with we preload all the most used routes right after the app is initially loaded so the user does not need to wait for it to load.
  - using OnPush change detection, as we are using the async pipe there is no need to manually run a change detection cycle.

    - this performs better as angular will not be querying updates all the time the user interacts with the screen (or other ui events that can trigger a change detection using default change detection strategy)
    - In this case we don't need to write any change detection logic and still benefit of better performance

  - chart.component.ts

    ```
    chart: {
      title: string;
      type: string;
      data: Array<Array<string | number>>;
      columnNames: string[];
      options: any;
    };
    ```

    - can be in an interface and be an optional `@Input()` this way it can be overridden by a caller component
    - not a requirement, but I thought that would be more extensible this way

  - added default schematics options for changeDetection: OnPush and scss styles.
  - NgRx:
    - finished implementing selectedSymbol
    - implemented loading indicator on the state
    - search for `REMARKS:` on the codebase to see the implementation details

* What I would still change/fix:
  - as we are using ngrx I would use the router store so we can use ngrx to query routes and use the redux tools to develop/troubleshoot
    - I did something similar with Akita router store recently
  - Upgrade all to angular 9 - and other libraries
  - Write more e2e and unit tests and change the current to wait for the actual api calls instead of wait for 3 seconds.
  - I would setup husky for git hooks to make sure we can only push code that is listed and passing unit tests.
  - I personally don't think this particular app benefit a lot of using NgRx, I would prefer to use Akita, especially for something this size as it requires much less boilerplate code.
    - Not an issue, I believe both ngrx and akita perform very well, or even plain rxjs subjects for an app the size of this code challenge
    - Recently I had done a poc with different state management approaches for a framework in angular, I compared both solutions, and it was very hard to encapsulate/abstract ngrx within the framework, Akita was very easy
      - As a study I wrote a simple state management solution based on Observable data service pattern
      - https://github.com/rodrigovillaca/rxjs-observable-state
        - this is based on this concept: https://coryrylan.com/blog/angular-observable-data-services
          - but using dictionaries instead of arrays so we had individual observables
          - This is just a sample study code, I didn't finish cleanup or wrote any tests, add proper error handling, types, etc... as it was something I did to study rxjs and typescript
          - I used it during this poc to compare state management libs vs plain rxjs
          - I thought was good to mention, definitely not production code.
          - This is based on some patterns I've been using for simple state management using ReplaySubject.
          - I would add a global error handler to handle logging, notification, etc
          - In real-world would add an http interceptor to handle http errors for things like: logout/cleanup if there is an expired token response

3. Are there any code smells or problematic implementations?

- libs/shared/ui/chart/src/lib/chart

  - chart.component.html

    - `*ngIf` statement is using inexistent property data
      - created the property replacing the existing observable data\$ property

  - chart.component.ts

    - `data$` of type `Observable<any>`

      - we can change it to `Observable<(string | number)[][]>` to match the object
      - I actually think it's better to have a data property of type `(string | number)[][]` and use async on the calling component instead of having an observable input, it works the same either way but the last one will be more consistent with the industry standards and would allow more flexibility if you are using other data source like a promise for example.

    - not unsubscribing from `data$`

      - I prefer use async pipe unless we have a good reason to handle the subscription

        - in situations we can't use the async pipe and need to subscribe on the component (like to preload data on a reactive form) I like to use https://github.com/ngneat/until-destroy to handle the un-subscription.
        - normally as a thumb rule for me when I am writing rxjs subscription code on typescript I always add `pipe(untilDestroyed(this))` or `pipe(first())`, the last one for subscriptions that we want to emit just once, both those does not need to call unsubscribe manually.
        - refactored the component, now using async pipe at the caller

- libs/stocks/data-access-price-query/src/lib/+state/price-query.facade.ts
  - I noticed the `skip(1)` operator, I assume is there as the first time the observable emits is an empty array
  - I confirmed that the observable emits an empty array the first time by using a debugger and adding a breakpoint at price-query.facade.ts line 15.
    - I changed this to `skipWhile(result => !result || !result.length)` so we are taking the decision to skip based on the data not on the order itself , that's important if the data is there when it emits the first time.
    - For example if this library is used in an app that uses some caching on the device storage (Like IndexDB/SQLLite/localStorage) the data might be preloaded on the first time it emits
      - Of course in some cases, skip(1) would be desired, depending on requirements, but this seems more appropriate for this implementation
      - In a production code I would probably add some other validations there
        - maybe throwErrors in the observable chain for bad data, or ignore it, or set a default value
          - This is hypothetical, we might not need/want to throw errors here, it would depend on the overall architecture
          - I would probably add the error handling in a map pipe before the skipWhile pipe, so it would likely read better, it would work both ways.
- Error Handling

  - The following code is redundant:

    ```
      *ngIf="
        !stockPickerForm.get('symbol').valid &&
        stockPickerForm.get('symbol').touched
      "
    ```

    - I removed it

  - `mat-form-field` will only display `mat-error` using the condition above by default. This default behavior can be overridden by using a custom ErrorStateMatcher at the input.

  - Added proper error handling for ngrx state and display errors on the screen

  - Added a example async validator to validate the symbol input field against the api so we can set the error message to the mat-error related to the symbol input
    - for some cases this might not be desired, as we call the server as the user changes the field. There is a debounce time to prevent too many calls, but depending on usage and api requirements/limitations this might not be the ideal.

### Task 2

```
Business requirement: As a user I should be able to type into
the symbol field and make a valid time-frame selection so that
the graph is refreshed automatically without needing to click a button.
```

_**Make a PR from the branch `feat_stock_typeahead` to `master` and provide a code review on this PR**_

> Add comments to the PR. Focus on all items that you can see - this is a hypothetical example but let's treat it as a critical application. Then present these changes as another commit on the PR.

#### Solution

My understanding is that I was supposed to do a code review on existing
feat_stock_typeahead branch but I could not find it on the repository.

Instead, I did one commit with a subscription memory leak and also is not very testable and have no tests. This commit would pass on QA as it does implement the requirements.

I provided the code review and fix for this commit next.

Fixed:

- Added tests and query mocks
- The subscription needs to unsubscribed on destroy to avoid a memory leak.
- We should be using valueChanges on the typescript file, it's more elegant and easy to test.

Bad commit: `04962f3ebffd8a9950bc2e05d868f10b67fae229`

Fix: `ba21d982251682aa96e54e76d0b10323ba1bd8b5`

For more details search the code repository for `REMARKS: task2`.

### Task 3

```
Business requirement: As a user I want to choose custom dates
so that I can view the trends within a specific period of time.
```

_**Implement this feature and make a PR from the branch `feat_custom_dates` to `master`.**_

> Use the material date-picker component

> We need two date-pickers: "from" and "to". The date-pickers should not allow selection of dates after the current day. "to" cannot be before "from" (selecting an invalid range should make both dates the same value)

#### Solution

Added filtering to component using picker value changes feeding a filtering rxjs subject, the filtering is wrapped in an expansion panel.

The way I coded the user will not be able to select an invalid value, and the filter will only be applied for valid date ranges (more than 1 day required). If needed we can revert this to use a custom validator at the form level an a subscription to value changes to update the form controls value for the being same date.

As we are doing UI filtering, I added the logic at the component, that's how I am used to work with Akita as it provides a filter plugin meant to be used from the component. I did manually using an rxjs subject, but I would look for something similar on NgRx in production, could be at the component or at state related code (follow the standard on the adopted solution).

For more details search the code repository for `REMARKS: task3`.

### Task 4

```
Technical requirement: the server `stocks-api` should be used as a proxy
to make calls. Calls should be cached in memory to avoid querying for the
same data. If a query is not in cache we should call-through to the API.
```

_**Implement the solution and make a PR from the branch `feat_proxy_server` to `master`**_

> It is important to get the implementation working before trying to organize and clean it up.

#### Solution

Installed h2o2, that's the most used reverse proxy solution for hapi.

Made the unit tests pass with the new url.

Testing have one caveat I could not fix. Currently, e2e tests require the stocks: api to be up, but unit testing no uses the same configuration. For this exercise I will let it like this as it would take time for me to study hapi better as it was the first time I worked with it.

To fix the above, I would need to study more hapi to see the the best way to use mock the config file that the proxy server is using to get host/port. There is not a lot of documentation on properly testing hapi.

The backend http calls are log on the hapi server console, so we can see the caching in action while using the app.

For more details search the code repository for `REMARKS: task4`.
