import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';
import {
  StocksAppConfig,
  StocksAppConfigToken
} from '@coding-challenge/stocks/data-access-app-config';
import { of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  // REMARKS: I normally wrap the http calls in a separate service, with a more in-depth url selection
  // normally I would have a ts or json file with the urls, recently I did this in a framework for micro-services
  // using a json config file (as per client devops requirement)
  // for this example I am doing something more simple, just moved the existing call
  // and adding a call for doing async validation of the symbol field
  constructor(
    @Inject(StocksAppConfigToken) private env: StocksAppConfig,
    private httpClient: HttpClient
  ) {}

  validateSymbol(symbol: string) {
    // REMARKS: I briefly looked at the iex api docs and fond that this api have less data and would be better for async validation
    // Also in a real world app we would likely a more in-depth error message handling, for an example I am just considering
    // the symbol is invalid for any error
    return this.httpClient.get(`${this.env.apiURL}/quote/${symbol}`).pipe(
      map(response => !!response),
      catchError(() => {
        return of(false);
      })
    );
  }

  fetchQuote(symbol: string, period: string) {
    return this.httpClient.get(`${this.env.apiURL}/chart/${symbol}/${period}`);
  }
}
