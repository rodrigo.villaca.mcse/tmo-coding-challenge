import { HttpErrorResponse } from '@angular/common/http';
import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { transformPriceQueryResponse } from './price-query-transformer.util';
import { PriceQueryAction, PriceQueryActionTypes } from './price-query.actions';
import { PriceQuery } from './price-query.type';

export const PRICEQUERY_FEATURE_KEY = 'priceQuery';

export interface PriceQueryState extends EntityState<PriceQuery> {
  selectedSymbol: string;
  // REMARKS: added an error property to the state, this could just be HttpErrorResponse, I added the other types as
  // errorHandlingOperator handles different error types, and this allows to handle errors from non-httpClient sources, like websocket,, or push notifications for a mobile app.
  error?: Error | HttpErrorResponse | string;
  loading?: boolean;
}

export function sortByDateNumeric(a: PriceQuery, b: PriceQuery): number {
  return a.dateNumeric - b.dateNumeric;
}

export const priceQueryAdapter: EntityAdapter<PriceQuery> = createEntityAdapter<
  PriceQuery
>({
  selectId: (priceQuery: PriceQuery) => priceQuery.dateNumeric,
  sortComparer: sortByDateNumeric
});

export interface PriceQueryPartialState {
  readonly [PRICEQUERY_FEATURE_KEY]: PriceQueryState;
}

export const initialState: PriceQueryState = priceQueryAdapter.getInitialState({
  selectedSymbol: ''
});

export function priceQueryReducer(
  state: PriceQueryState = initialState,
  action: PriceQueryAction
): PriceQueryState {
  switch (action.type) {
    case PriceQueryActionTypes.PriceQueryFetched: {
      return priceQueryAdapter.addAll(
        transformPriceQueryResponse(action.queryResults),
        { ...state, error: null, loading: false }
      );
    }
    case PriceQueryActionTypes.FetchPriceQuery: {
      return {
        ...state,
        loading: true
      };
    }
    case PriceQueryActionTypes.PriceQueryFetchError: {
      return {
        ...state,
        loading: false
      };
    }
    case PriceQueryActionTypes.SelectSymbol: {
      return {
        ...state,
        selectedSymbol: action.symbol
      };
    }
    // REMARKS: added reducer for http error
    case PriceQueryActionTypes.PriceQueryFetchError: {
      return {
        ...state,
        error: action.error
      };
    }
  }
  return state;
}
