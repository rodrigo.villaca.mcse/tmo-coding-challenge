import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import {
  GoogleChartComponent,
  GoogleChartsModule
} from 'angular-google-charts';
import { ChartComponent } from './chart.component';
import { DEFAULT_CHART_OPTIONS } from './default-chart-options.constant';

const quotes = [
  ['2020-01-02', 307.5],
  ['2020-01-02', 307.5],
  ['2020-01-02', 312.3]
];

describe('ChartComponent', () => {
  let component: ChartComponent;
  let fixture: ComponentFixture<ChartComponent>;
  let chartElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [GoogleChartsModule.forRoot()],
      declarations: [ChartComponent]
    }).compileComponents();
  }));
  beforeEach(() => {
    fixture = TestBed.createComponent(ChartComponent);
    component = fixture.componentInstance;
    component.data = quotes;

    fixture.detectChanges();
    chartElement = fixture.debugElement.query(By.css('google-chart'));
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should initialize', () => {
    component.ngOnInit();

    expect(component.chartOptions).toEqual(DEFAULT_CHART_OPTIONS);
  });

  it('should set correct options', () => {
    const title = 'test-title';
    component.options = { title };
    component.ngOnInit();

    expect(component.chartOptions).toEqual({
      ...DEFAULT_CHART_OPTIONS,
      ...component.options
    });
  });

  it('should set data and options for google-chart component', () => {
    const chart = chartElement.componentInstance as GoogleChartComponent;

    expect(component.chartOptions.options).toEqual(chart.options);
    expect(component.chartOptions.columnNames).toEqual(chart.columnNames);
    expect(component.chartOptions.type).toEqual(chart.type);
    expect(component.data).toEqual(chart.data);
  });
});
