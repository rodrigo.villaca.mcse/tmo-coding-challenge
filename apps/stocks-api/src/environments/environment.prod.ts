import { StocksAppConfig } from '@coding-challenge/stocks/data-access-app-config';

export const environment: StocksAppConfig & {
  ttl: number;
  port: number;
  host: string;
} = {
  production: true,
  apiKey: 'Tpk_226ff0c140af410cb63f9f27812a2bbf',
  apiURL: 'https://sandbox.iexapis.com',
  ttl: 30 * 60 * 1000,
  port: 3333,
  host: 'localhost'
};
