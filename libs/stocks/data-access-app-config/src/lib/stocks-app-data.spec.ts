import { StocksAppConfigToken } from '..';

describe('StocksAppData', () => {
  it('have injection token', () => {
    expect(StocksAppConfigToken).toBeDefined();
  });
});
