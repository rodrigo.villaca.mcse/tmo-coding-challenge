import { server } from './main';
describe('stock api', () => {
  beforeAll(async done => {
    server.events.on('start', () =>
      setTimeout(() => {
        done();
      }, 2000)
    );
  });

  afterAll(done => {
    server.events.on('stop', () => {
      done();
    });
    server.stop();
  });

  // REMARKS: task4 - this tests the proxy connection
  test('should success with server connection', async function() {
    const options = {
      method: 'GET',
      url: 'http://localhost:3333/chart/tmus/1m'
    };
    const data = await server.inject(options);
    expect(data.statusCode).toBe(200);
  });
});
