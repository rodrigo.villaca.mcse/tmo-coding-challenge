import { AbstractControl } from '@angular/forms';
import { of } from 'rxjs';

export class SymbolValidatorMock {
  validateSymbol() {
    return (control: AbstractControl) => of(null);
  }
}
