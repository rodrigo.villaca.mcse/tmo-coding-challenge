export interface ChartOptions {
  title: string;
  type: string;
  data: Array<Array<string | number>>;
  columnNames: string[];
  options: { title: string; width: number; height: number };
}
