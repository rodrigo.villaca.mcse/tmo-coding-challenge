import { Component } from '@angular/core';
import { TITLE } from './title.constant';
@Component({
  selector: 'coding-challenge-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = TITLE;
}
