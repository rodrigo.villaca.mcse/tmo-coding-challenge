import { HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';
import { map, skipWhile } from 'rxjs/operators';
import { FetchPriceQuery, SelectSymbol } from './price-query.actions';
import { PriceQueryPartialState } from './price-query.reducer';
import {
  getAllPriceQueries,
  getLoading,
  getPriceQueriesError,
  getSelectedSymbol
} from './price-query.selectors';

// REMARKS: this is an example error handling for mapping store errors to actual messages,
// if the app have multiple calls using the same response format a this custom rxjs operator can be in a separate file on a shared module
const errorHandlingOperator = () =>
  map((error: string | Error | HttpErrorResponse) => {
    if (!error) {
      return null;
    } else if (typeof error === 'string') {
      return error;
    } else if (error instanceof HttpErrorResponse) {
      return typeof error.error === 'string' ? error.error : 'Unknown Error';
    } else if (error instanceof Error) {
      return error.message;
    } else {
      return 'Unknown Error';
    }
  });

@Injectable()
export class PriceQueryFacade {
  // REMARKS: added ngrx loading indicator
  loading$ = this.store.pipe(select(getLoading));

  selectedSymbol$ = this.store.pipe(select(getSelectedSymbol));

  error$ = this.store
    .select(getPriceQueriesError)
    .pipe(errorHandlingOperator());

  priceQueries$ = this.store.pipe(
    select(getAllPriceQueries),
    skipWhile(result => !result || !result.length),
    map(priceQueries =>
      priceQueries.map(priceQuery => [priceQuery.date, priceQuery.close])
    )
  );

  constructor(private store: Store<PriceQueryPartialState>) {}

  fetchQuote(symbol: string, period: string) {
    this.store.dispatch(new FetchPriceQuery(symbol, period));
  }

  // REMARKS: finished implementing selectedSymbol - dispatch a new SelectSymbol action
  selectSymbol(symbol: string) {
    this.store.dispatch(new SelectSymbol(symbol.toUpperCase()));
  }
}
