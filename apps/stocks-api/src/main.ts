/**
 * Reverse proxy with caching for the stocks application
 **/
import { ResponseObject, Server } from 'hapi';
import { environment } from './environments/environment';

const H2o2 = require('@hapi/h2o2');
const apiRoot = `${environment.apiURL}/beta/stock`;
const token = `token=${environment.apiKey}`;

export const server = new Server({
  port: environment.port,
  host: environment.host,
  routes: { cors: true }
});

export const init = async () => {
  // REMARKS: task4 - added H2o2 for proxy handling. Installed npm package @hapi/h2o2
  await server.register(H2o2);
  await server.start();

  // REMARKS: task4 - routes bellow are used by the application
  server.route({
    method: 'GET',
    path: '/quote/{symbol}',
    handler: (request, h: any) =>
      h.proxy({
        uri: `${apiRoot}/${request.params.symbol}/quote?${token}`,
        passThrough: true
      }),
    options: { cache: { privacy: 'public', expiresIn: environment.ttl } }
  });

  server.route({
    method: 'GET',
    path: '/chart/{symbol}/{period}',
    handler: (request, h: any) =>
      h.proxy({
        uri: `${apiRoot}/${request.params.symbol}/chart/${
          request.params.period
        }?${token}`,
        passThrough: true
      }),
    options: { cache: { privacy: 'public', expiresIn: environment.ttl } }
  });
  console.log('Server running on %s', server.info.uri);
};

server.events.on('response', request => {
  const response = request.response as ResponseObject;
  // REMARKS: task4 - logging only server responses from backend api
  console.log(
    `${request.info.remoteAddress} : ${request.method.toUpperCase()}  ${
      request.path
    }  --> ${response.statusCode} `
  );
});
process.on('unhandledRejection', err => {
  console.log(err);
  process.exit(1);
});

init();
