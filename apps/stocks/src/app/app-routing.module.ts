import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { StocksFeatureShellModule } from '@coding-challenge/stocks/feature-shell';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => StocksFeatureShellModule
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {}
